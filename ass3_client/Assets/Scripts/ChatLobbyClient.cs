﻿using shared;
using shared.src.model;
using shared.src.protocol;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

/**
 * The main ChatLobbyClient where you will have to do most of your work.
 * 
 * @author J.C. Wichman
 */

public struct SizeHelper
{
    public static int INT_SIZE = sizeof(int);
}

public class ChatLobbyClient : MonoBehaviour
{
    //TODO refactor sending objects code


    //reference to the helper class that hides all the avatar management behind a blackbox
    private AvatarAreaManager _avatarAreaManager;
    //reference to the helper class that wraps the chat interface
    private PanelWrapper _panelWrapper;

    [SerializeField] private string _server = "localhost";
    [SerializeField] private int _port = 55555;
    [SerializeField] private int _id;

    private TcpClient _client;
    private List<AvatarProtocol> activeAvatars = new List<AvatarProtocol>();

    private void Start()
    {
        connectToServer();

        //register for the important events
        _avatarAreaManager = FindObjectOfType<AvatarAreaManager>();
        _avatarAreaManager.OnAvatarAreaClicked += onAvatarAreaClicked;

        _panelWrapper = FindObjectOfType<PanelWrapper>();
        _panelWrapper.OnChatTextEntered += onChatTextEntered;


    }

    private void connectToServer()
    {
        try
        {
            _client = new TcpClient();
            _client.Connect(_server, _port);
            Debug.Log("Connected to server.");
        }
        catch (Exception e)
        {
            Debug.Log("Could not connect to server:");
            Debug.Log(e.Message);
        }
    }

    //SENDING CODE

    private void onAvatarAreaClicked(Vector3 pClickPosition)
    {
        Debug.Log("ChatLobbyClient: you clicked on " + pClickPosition);
        Position newPosition = new Position(pClickPosition.x, pClickPosition.y, pClickPosition.z);
        MovePositionRequest positionRequest = new MovePositionRequest(newPosition);
        sendObject(positionRequest);

    }

    private void onChatTextEntered(string pText)
    {
        _panelWrapper.ClearInput();
        sendMessage(pText);
    }

    private void sendObject(ISerializable pObject)
    {
        Debug.Log("Sending: " + pObject.ToString());
        Packet outPacket = new Packet();
        outPacket.Write(pObject);
        byte[] outData = outPacket.GetBytes();
        StreamUtil.Write(_client.GetStream(), outData);
    }

    private void sendMessage(string pOutString)
    {
        try
        {
            Debug.Log("Sending:" + pOutString);
            SimpleMessage message = new SimpleMessage();
            message.text = pOutString;
            sendObject(message);
        }
        catch (Exception e)
        {
            //for quicker testing, we reconnect if something goes wrong.
            Debug.Log(e.Message);
            _client.Close();
            connectToServer();
        }
    }

    //END SENDING CODE

    // RECEIVING CODE
    private void Update()
    {
        try
        {
            if (_client.Available > 0)
            {
                byte[] inBytes = StreamUtil.Read(_client.GetStream());
                Packet inPacket = new Packet(inBytes);
                //Debug.Log("this is before reading object");
                ISerializable inObject = inPacket.ReadObject();
                processIncomingObject(inObject);
            }
        }
        catch (Exception e)
        {
            //for quicker testing, we reconnect if something goes wrong.
            Debug.Log(e.Message);
            _client.Close();
            connectToServer();
        }
    }

    private void processIncomingObject(ISerializable inObject)
    {
        if (!(inObject is SimpleMessage))
        {
            Debug.Log("object received: " + inObject);
        }
        if (inObject is AvatarProtocol)
        {
            acceptJoinRequest(inObject as AvatarProtocol);
        }
        else if (inObject is AvatarUpdate)
        {
            acceptUpdateRequest(inObject as AvatarUpdate);
        }
        else if (inObject is RemoveAvatarRequest)
        {
            acceptFaultyClientProcess(inObject as RemoveAvatarRequest);
        }
        else if (inObject is SimpleMessage)
        {
            acceptMessage(inObject as SimpleMessage);
        }
        else if (inObject is MovePositionResult)
        {
            acceptPositionChange(inObject as MovePositionResult);
        }
        else if (inObject is SetSkinResult)
        {
            acceptSkinUpdate(inObject as SetSkinResult);
        }
    }

    private void acceptPositionChange(MovePositionResult positionResult)
    {
        Debug.Log("New Position Result: " + positionResult.updatedProtocol.position);
        Vector3 newPosition = new Vector3(positionResult.updatedProtocol.position.x,
            positionResult.updatedProtocol.position.y,
            positionResult.updatedProtocol.position.z);
        _avatarAreaManager.GetAvatarView(positionResult.updatedProtocol.id).Move(newPosition);

    }

    private void acceptFaultyClientProcess(RemoveAvatarRequest removeAvatarRequest)
    {
        Debug.Log("Removed client " + removeAvatarRequest.avatarProtocol.id);
        _avatarAreaManager.RemoveAvatarView(removeAvatarRequest.avatarProtocol.id);
        activeAvatars.Remove(removeAvatarRequest.avatarProtocol);
    }

    private void acceptMessage(SimpleMessage message)
    {
        if (message.text.Length == 0) return;
        showMessage(message.id, message.text);
    }

    private void acceptUpdateRequest(AvatarUpdate avatarUpdate)
    {
        Debug.Log("Accepting an AvatarUpdate.");
        List<AvatarProtocol> updateList = avatarUpdate.avatars.Where(a => !activeAvatars.Any(t => t.id == a.id)).ToList();
        foreach (AvatarProtocol avatar in updateList)
        {
            Debug.Log("Avatar to add: " + avatar);
            AvatarView view = _avatarAreaManager.AddAvatarView(avatar.id);
            view.transform.position = new Vector3(avatar.position.x, avatar.position.y, avatar.position.z);
            view.SetSkin(avatar.skinId);
            activeAvatars.Add(avatar);
            Debug.Log("Added avatar " + avatar.id + " to activeAvatars list");
            Debug.Log("AvatarAreaManager list count: " + _avatarAreaManager.GetAllAvatarIds().Count);
        }
    }

    private void acceptSkinUpdate(SetSkinResult skinResult)
    {
        Debug.Log("Processing SetSkinResult");       
        _avatarAreaManager.GetAvatarView(skinResult.updatedAvatar.id).SetSkin(skinResult.updatedAvatar.skinId);
        Debug.Log("Avatar with id: " + skinResult.updatedAvatar.id + " changed his skin.");
    }

    private void acceptJoinRequest(AvatarProtocol avatarProtocol)
    {
        Debug.Log("Accepting a JoinRequest");
        Vector3 position = new Vector3(avatarProtocol.position.x, avatarProtocol.position.y, avatarProtocol.position.z);
        AvatarView view = _avatarAreaManager.AddAvatarView(avatarProtocol.id);
        _id = avatarProtocol.id;
        view.transform.position = position;
        view.SetSkin(avatarProtocol.skinId);
        activeAvatars.Add(avatarProtocol);
        view.ShowRing();
        Debug.Log("Client id: " + _id);
        Debug.Log("Client position: " + position);
        Debug.Log("Added avatar " + avatarProtocol.id + " to activeAvatars list");

    }

    private void showMessage(int id, string text)
    {
        Debug.Log("Message to be shown: " + text);
        AvatarView avatarView = _avatarAreaManager.GetAvatarView(id);
        avatarView.Say(text);
    }

}
