﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shared.src.model
{
    public class Position : ISerializable
    {
        public float x, y, z;

        public Position()
        {

        }

        public Position(float pX, float pY, float pZ)
        {
            x = pX;
            y = pY;
            z = pZ;
        }

        public void Serialize(Packet pPacket)
        {
            writePosition(x, y, z, pPacket);
        }

        public void Deserialize(Packet pPacket)
        {
            readPosition(pPacket);
        }

        private void readPosition(Packet pPacket)
        {
            if (pPacket != null)
            {
                x = pPacket.ReadFloat();
                y = pPacket.ReadFloat();
                z = pPacket.ReadFloat();
            }
            else
            {
                Console.WriteLine(pPacket.ToString() + " doesn't exist wat.");
                return;
            }
        }
        private void readRotation(Packet pPacket)
        {
            //TODO add rotation to be read
        }

        private void writePosition(float pX, float pY, float pZ, Packet pPacket)
        {
            if (pPacket != null)
            {
                pPacket.Write(pX);
                pPacket.Write(pY);
                pPacket.Write(pZ);
            }
            else
            {
                Console.WriteLine(pPacket.ToString() + " doesn't exist wat.");
                return;
            }
        }
        private void writeRotation(Packet pPacket)
        {
            //TODO add rotation to be written
        }

        public float getLength()
        {
            return (float)Math.Sqrt(x * x + y * y + z * z);
        }

        public static Position operator*(Position transform, float multiplier)
        {
            Position newTransform = new Position(transform.x * multiplier, transform.y * multiplier, transform.z * multiplier);
            return newTransform;
        }


        public override string ToString()
        {
            return $"X:{x}, Y:{y}, Z:{z}";
        }

        public bool Equals(Position other)
        {
            return Math.Abs(x - other.x) <= 0.1f && Math.Abs(y-other.y) <= 0.1f && Math.Abs(z - other.z) <= 0.1f;
        }

    }
}
