﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using shared.src.model;

namespace shared.src.protocol
{
    public class MovePositionRequest : ISerializable
    {
        public Position newPosition;

        public MovePositionRequest()
        {

        }

        public MovePositionRequest(Position pNewPosition)
        {
            newPosition = pNewPosition;
        }

        public void Serialize(Packet pPacket)
        {
            pPacket.Write(newPosition);
        }

        public void Deserialize(Packet pPacket)
        {
            newPosition = pPacket.ReadObject() as Position;
        }
    }
}
