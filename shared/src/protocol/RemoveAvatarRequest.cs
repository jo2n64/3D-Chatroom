﻿using shared.src.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shared.src.protocol
{
    public class RemoveAvatarRequest : ISerializable
    {
        public AvatarProtocol avatarProtocol;

        public void Serialize(Packet pPacket)
        {
            pPacket.Write(avatarProtocol);
        }

        public void Deserialize(Packet pPacket)
        {
            avatarProtocol = pPacket.ReadObject() as AvatarProtocol;
        }
    }
}
