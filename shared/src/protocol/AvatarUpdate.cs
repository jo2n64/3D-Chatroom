﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shared.src.protocol
{
    public class AvatarUpdate : ISerializable
    {
        public List<AvatarProtocol> avatars;

        public void Serialize(Packet pPacket)
        {
            int count = avatars.Count;
            pPacket.Write(count);
            for(int i = 0; i < avatars.Count; i++)
            {
                pPacket.Write(avatars[i]);
            }
        }

        public void Deserialize(Packet pPacket)
        {
            avatars = new List<AvatarProtocol>();
            int count = pPacket.ReadInt();
            for(int i = 0; i < count; i++)
            {
                avatars.Add(pPacket.Read<AvatarProtocol>());
            }
        }
    }
}
