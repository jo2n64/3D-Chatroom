﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shared.src.protocol
{
    public class SetSkinResult : ISerializable
    {
        public AvatarProtocol updatedAvatar;

        public SetSkinResult()
        {

        }

        public SetSkinResult(AvatarProtocol pUpdatedAvatar)
        {
            updatedAvatar = pUpdatedAvatar;
        }

        public void Serialize(Packet pPacket)
        {
            pPacket.Write(updatedAvatar);
        }

        public void Deserialize(Packet pPacket)
        {
            updatedAvatar = pPacket.ReadObject() as AvatarProtocol;
        }
    }
}
