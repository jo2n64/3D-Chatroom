﻿using shared.src.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shared.src.protocol
{
    public class AvatarProtocol : ISerializable
    {
        public int id;
        public int skinId;
        public Position position;

        public AvatarProtocol() { }

        public AvatarProtocol(int pId, Position pPosition, int pSkinId)
        {
            id = pId;
            position = pPosition;
            skinId = pSkinId;
        }

        public void Serialize(Packet pPacket)
        {
            pPacket.Write(id);
            pPacket.Write(position);
            pPacket.Write(skinId);
        }

        public void Deserialize(Packet pPacket)
        {
            id = pPacket.ReadInt();
            position = pPacket.ReadObject() as Position;
            skinId = pPacket.ReadInt();
        }

        public override string ToString()
        {
            return $"AVATAR PROTOCOL - id:{id}, position:{position}, skinId:{skinId}";
        }
    }
}
