﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shared.src.protocol
{   
    public class MovePositionResult : ISerializable
    {
        public AvatarProtocol updatedProtocol;

        public MovePositionResult()
        {

        }

        public MovePositionResult(AvatarProtocol pUpdatedProtocol)
        {
            updatedProtocol = pUpdatedProtocol;
        }

        public void Serialize(Packet pPacket)
        {
            pPacket.Write(updatedProtocol);
        }

        public void Deserialize(Packet pPacket)
        {
            updatedProtocol = pPacket.ReadObject() as AvatarProtocol;
        }
    }
}
