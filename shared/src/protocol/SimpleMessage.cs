﻿namespace shared
{
    public class SimpleMessage : ISerializable
    {
        public string text;
        public int id;


        public SimpleMessage() { }
        public SimpleMessage(int pId, string pText)
        {
            id = pId;
            text = pText;
        }
        public void Serialize(Packet pPacket)
        {
            pPacket.Write(id);
            pPacket.Write(text);
        }

        public void Deserialize(Packet pPacket)
        {
            id = pPacket.ReadInt();
            text = pPacket.ReadString();
        }
    }
}
