﻿using server;
using shared;
using shared.src.protocol;
using shared.src.model;
using System;
using System.Net.Sockets;
using System.Net;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

/**
 * This class implements a simple tcp echo server.
 * Read carefully through the comments below.
 * Note that the server does not contain any sort of error handling.
 */
class TCPServerSample
{
    public static void Main(string[] args)
    {
        TCPServerSample server = new TCPServerSample();
        server.run();
    }

    private TcpListener _listener;
    private int idIterator = 0;
    private Dictionary<TcpClient, AvatarProtocol> _clients = new Dictionary<TcpClient, AvatarProtocol>();
    private void run()
    {
        Console.WriteLine("Server started on port 55555\n");

        _listener = new TcpListener(IPAddress.Any, 55555);
        _listener.Start();

        while (true)
        {
            try
            {
                processNewClients();
                processExistingClients();
                processFaultyClients();
                //Although technically not required, now that we are no longer blocking, 
                //it is good to cut your CPU some slack
                Thread.Sleep(100);
            }
            catch (Exception e)
            {
                Console.WriteLine("Some generic problem happened! " + e.Message);
            }
        }
    }

    private void processNewClients()
    {
        try
        {
            while (_listener.Pending())
            {
                Console.WriteLine("Accepting a new client...");
                TcpClient acceptedClient = _listener.AcceptTcpClient();

                int id = idIterator;
                Console.WriteLine("Id generated: " + id);

                int skinId = ServerMathHelper.getRandomSkinId();
                Console.WriteLine("SkinId generated: " + skinId);

                Position randomPosition = ServerMathHelper.getRandomPosition();
                Console.WriteLine("Position generated: " + randomPosition);

                AvatarProtocol avatar = new AvatarProtocol(id, randomPosition, skinId);
                Console.WriteLine("AvatarProtocol generated: " + avatar.ToString());

                _clients.Add(acceptedClient, avatar);
                Console.WriteLine("Client added.");

                handleJoinRequest(acceptedClient, avatar);

                Console.WriteLine("Handling an avatar update");
                foreach (TcpClient client in _clients.Keys)
                {
                    handleAvatarUpdate(client);
                }
                Console.WriteLine("Clients in scene: " + _clients.Count);
                idIterator++;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("Something while accepting clients happened: " + e.Message);
        }
    }

    private void processExistingClients()
    {
        foreach (TcpClient client in _clients.Keys.ToList())
        {
            try
            {
                if (client.Available == 0) continue;
                byte[] data = StreamUtil.Read(client.GetStream());
                Packet inPacket = new Packet(data);
                ISerializable inObject = inPacket.ReadObject();
                Console.WriteLine("Object read: " + inObject.GetType().ToString());
                processReceivedMessage(inObject, client);
            }
            catch (Exception e)
            {
                Console.WriteLine("Something while processing existing clients happened: " + e.Message);
            }
        }
    }
    private void processFaultyClients()
    {
        List<TcpClient> clientKeys = _clients.Keys.ToList();
        for (int i = _clients.Count - 1; i >= 0; i--)
        {
            if (i >= _clients.Count) continue;
            TcpClient client = clientKeys[i];
            try
            {
                SimpleMessage simpleMessage = new SimpleMessage();
                simpleMessage.text = "";
                sendObject(client, simpleMessage);
            }
            catch
            {
                removeClient(client);
            }
        }
    }

    private void processReceivedMessage(ISerializable inObject, TcpClient client)
    {
        if (inObject is SimpleMessage)
        {
            processMessageSending(inObject as SimpleMessage, _clients[client]);
        }
        else if (inObject is MovePositionRequest)
        {
            handlePositionRequest(inObject as MovePositionRequest, _clients[client]);
        }
    }

    private void processMessageSending(SimpleMessage message, AvatarProtocol avatar)
    {
        try
        {
            if (message.text.StartsWith("/whisper"))
            {
                Console.WriteLine("Handling a whisper message");
                SimpleMessage newMessage = new SimpleMessage();
                newMessage.text = message.text.Substring(9); //doesnt handle empty strings/spaces
                Console.WriteLine("Whispered message: " + newMessage.text + ".\n");
                float avatarLength = avatar.position.getLength();
                handleWhisperMessage(avatarLength, newMessage, avatar);
            }
            else if (message.text.StartsWith("/setskin"))
            {
                handleSkinUpdate(avatar);
            }
            else
            {
                sendMessagesToAll(message, avatar.id);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("Something while processing a chat message happened.");
            Console.WriteLine(e.Message + "\n");
        }
    }

    private void handlePositionRequest(MovePositionRequest positionRequest, AvatarProtocol avatar)
    {
        try
        {
            Console.WriteLine("Creating a MovePositionResult.");
            MovePositionResult positionResult = new MovePositionResult();
            avatar.position = positionRequest.newPosition;
            positionResult.updatedProtocol = avatar;
            Console.WriteLine($"Avatar with id {avatar.id} moved to {positionResult.updatedProtocol.position}\n");
            sendPositionUpdateToAll(positionResult);
        }
        catch (Exception e)
        {
            Console.WriteLine("Something while processing position happened: " + e.Message);
        }

    }

    private void handleFaultyClientRequest(TcpClient client, AvatarProtocol avatar)
    {
        try
        {
            RemoveAvatarRequest removeAvatarRequest = new RemoveAvatarRequest();
            removeAvatarRequest.avatarProtocol = avatar;
            sendObject(client, removeAvatarRequest);
        }
        catch (Exception e)
        {
            Console.WriteLine("Something during avatar removal happened! " + e.Message);
        }
    }

    private void handleMessageRequest(TcpClient pClient, ISerializable pObject, int id)
    {
        try
        {
            SimpleMessage message = pObject as SimpleMessage;
            message.id = id;
            Console.WriteLine("Message sent: " + message.text + " from client id " + message.id + "\n");
            sendObject(pClient, pObject);
        }
        catch (Exception e)
        {
            Console.WriteLine("Something while sending messages happened: " + e.Message);
        }
    }

    private void handleAvatarUpdate(TcpClient pClient)
    {
        try
        {
            Console.WriteLine("Constructing an avatar update\n");
            AvatarUpdate avatarUpdate = new AvatarUpdate();
            avatarUpdate.avatars = _clients.Values.ToList();
            sendObject(pClient, avatarUpdate);
        }
        catch (Exception e)
        {
            Console.WriteLine("Something during avatar updating happened! " + e.Message);
        }
    }

    private void handleJoinRequest(TcpClient pClient, AvatarProtocol avatar)
    {
        try
        {
            sendObject(pClient, avatar);
            Console.Write($"Client joined with id {avatar.id}.");
            Console.WriteLine($"Position: {avatar.position}\n");
        }
        catch (Exception e)
        {
            Console.WriteLine("Something while doing the join request happened: " + e.Message);
        }
    }

    private void handleWhisperMessage(float avatarLength, SimpleMessage newMessage, AvatarProtocol avatar)
    {
        foreach (TcpClient client in _clients.Keys)
        {
            float otherAvatarLength = _clients[client].position.getLength();
            if (Math.Abs(avatarLength - otherAvatarLength) < 2.0f)
            {
                handleMessageRequest(client, newMessage, avatar.id);
            }
        }
    }

    private void handleSkinUpdate(AvatarProtocol avatar)
    {
        Console.WriteLine("Changing skin on avatar with id " + avatar.id);
        int skinId = ServerMathHelper.getRandomSkinId();
        avatar.skinId = skinId;
        SetSkinResult setSkinResult = new SetSkinResult();
        setSkinResult.updatedAvatar = avatar;
        Console.WriteLine("New skin id: " + skinId + "\n");
        sendSkinUpdateToAll(setSkinResult);
    }

    private void sendSkinUpdateToAll(SetSkinResult setSkinResult)
    {
        Console.WriteLine("Sending skin update...\n");
        foreach (TcpClient client in _clients.Keys)
        {
            sendObject(client, setSkinResult);
        }
    }

    private void sendObject(TcpClient pClient, ISerializable pObject)
    {
        Packet outPacket = new Packet();
        outPacket.Write(pObject);
        byte[] data = outPacket.GetBytes();
        StreamUtil.Write(pClient.GetStream(), data);
    }

    private void sendPositionUpdateToAll(MovePositionResult positionResult)
    {
        foreach (TcpClient client in _clients.Keys)
        {
            sendObject(client, positionResult);
        }
    }

    private void sendMessagesToAll(ISerializable pObject, int id)
    {
        foreach (TcpClient client in _clients.Keys)
        {
            handleMessageRequest(client, pObject, id);
        }
    }

    private void removeClient(TcpClient client)
    {
        Console.WriteLine("Clients left before removal: " + _clients.Count);
        client.Close();
        Console.WriteLine("Client closed.");
        foreach (TcpClient clientLeft in _clients.Keys)
        {
            if (clientLeft != client)
            {
                handleFaultyClientRequest(clientLeft, _clients[client]);
            }
        }
        _clients.Remove(client);
        Console.WriteLine("Client hopefully disconnected.");
        Console.WriteLine("Clients left after removal: " + _clients.Count + "\n");
    }

}

