﻿using shared.src.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server
{
    public struct ServerMathHelper
    {
        public const float Deg2Rad = 0.0174532924F;

        public static Position getRandomPosition()
        {
            Random rand = new Random();
            float randomAngle = rand.Next(0, 180) * ServerMathHelper.Deg2Rad;
            float randomDistance = rand.Next(0, 10);
            return new Position((float)Math.Cos(randomAngle), 0, (float)Math.Sin(randomAngle)) * randomDistance;
        }

        public static int getRandomSkinId()
        {
            Random rand = new Random();
            int randomSkinId = rand.Next(0, 1000);
            return randomSkinId;
        }

        
    }
}
